import { Component, MouseEventHandler } from "react"
import './NotFound.css';

export interface INotFoundComponentProps{
}

export default class NotFoundComponent extends Component<{}, INotFoundComponentProps>{

    constructor(props: INotFoundComponentProps){
        super(props);
    }

    render() {
       return (<>NotFound</>);

    }
}