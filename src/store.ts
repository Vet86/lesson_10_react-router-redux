import { configureStore } from "@reduxjs/toolkit";
import { userSlice } from "./stateManagement/userSlice";
//import { legacy_createStore as createStore } from "redux";

// redux
/*const rootReducer = combineReducers({
  user: userReducer,
});*/

//const store = createStore(rootReducer);


//RTK
const store = configureStore({
  reducer: {
    user: userSlice.reducer,
    // И под thunk
    //funky: thunkReducer,
  },
});
export default store;
