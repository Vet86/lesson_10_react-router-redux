import { useState } from "react";
import './Auth.css';
import { useDispatch } from "react-redux";
import { userSet } from "../../stateManagement/userSlice";
import { Button } from "react-bootstrap";

export interface IAuthComponentProps{
    login: string;
    password: string;    
}

const AuthFunction = () =>
{
  const [ login, setLogin ] = useState('');
  const [ password, setPassword ] = useState('');
  const dispatch = useDispatch();

  const onLoginChanged = (e: any) => {
    console.log(e.target.value);
    setLogin(e.target.value);

    console.log('login: ' + login);
    console.log('password: ' + password);

  };
  
  const onPasswordChanged = (e: any) => {
    console.log(e.target.value);
    setPassword(e.target.value);
    
    console.log('login: ' + login);
    console.log('password: ' + password);
  };

  const onLoginClick = ()=> {
      dispatch(userSet(login));
      
      fetch('https://somewherepath.net/auth', {
          method: 'POST',
          headers: {
              
            },
          body: JSON.stringify({
              login: login,
              password: password
          })
      })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.error) {
          alert("Error Password or Username");
        } else {
          window.open(
            "target.html"
          );
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

     return (
     <div className="container-fluid align-items-center">
      <table cellSpacing="10" cellPadding="0">
      <tr>
          <td>Login</td>
          <td><input type="text" onChange={onLoginChanged} /></td>
      </tr>
      <tr>
          <td>Password</td>
          <td><input type="text" onChange={onPasswordChanged} /></td>
      </tr>
      </table>
      <br></br>
      <Button title="Login in hook" onClick={onLoginClick}>Login in hook</Button>

     </div> 

     
     );
}

export default AuthFunction;
